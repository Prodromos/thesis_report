\contentsline {chapter}{\numberline {1}Eισαγωγή}{17}
\contentsline {section}{\numberline {1.1}Κίνητρο}{17}
\contentsline {section}{\numberline {1.2}Περιγραφή του Προβλήματος}{19}
\contentsline {section}{\numberline {1.3}Σκοπός της Διπλωματικής}{20}
\contentsline {section}{\numberline {1.4}Διάρθρωση του Κειμένου}{21}
\contentsline {chapter}{\numberline {2}Υπόβαθρο}{23}
\contentsline {section}{\numberline {2.1}Θεωρητικό Υπόβαθρο}{23}
\contentsline {subsection}{\numberline {2.1.1}Φίλτρα Σωματιδίων}{23}
\contentsline {subsubsection}{Διαδικασία Λειτουργίας}{24}
\contentsline {subsubsection}{Μέθοδοι Επαναδειγματοληψίας (Resampling)}{25}
\contentsline {subsection}{\numberline {2.1.2}Βαθμονόμηση Κάμερας}{26}
\contentsline {subsubsection}{Ενδογενείς Παράμετροι}{26}
\contentsline {subsubsection}{Εξωγενείς Παράμετροι}{27}
\contentsline {subsection}{\numberline {2.1.3}Συνελικτικά Νευρωνικά Δίκτυα - CNNs}{28}
\contentsline {section}{\numberline {2.2}Τεχνολογικό Υπόβαθρο}{29}
\contentsline {subsection}{\numberline {2.2.1}Γλώσσα Προγραμματισμού}{29}
\contentsline {subsection}{\numberline {2.2.2}Robot Operating System (ROS)}{30}
\contentsline {subsubsection}{Πακέτο tf}{31}
\contentsline {subsubsection}{Πακέτο cv\_bridge}{32}
\contentsline {subsection}{\numberline {2.2.3}OpenCV}{32}
\contentsline {chapter}{\numberline {3}Eπισκόπηση Ερευνητικής Περιοχής}{35}
\contentsline {section}{\numberline {3.1}Εισαγωγή}{35}
\contentsline {section}{\numberline {3.2}Human Detection}{36}
\contentsline {section}{\numberline {3.3}Human Tracking}{42}
\contentsline {section}{\numberline {3.4}Προβλήματα}{45}
\contentsline {chapter}{\numberline {4}Yλοποίηση του Συστήματος}{47}
\contentsline {section}{\numberline {4.1}Ανίχνευση και Παρακολούθηση Ανθρώπινης Παρουσίας με Χρήση Μίας RGB Κάμερας}{47}
\contentsline {section}{\numberline {4.2}Ανίχνευση και Παρακολούθηση Ανθρώπινης Παρουσίας με Χρήση Πολλαπλών RBG Καμερών}{53}
\contentsline {subsection}{\numberline {4.2.1}Διαδικασία Υλοποίησης Φίλτρων Σωματιδίων σε 3D Χώρο με Πολλαπλές Κάμερες}{56}
\contentsline {subsubsection}{Εκτίμηση Απόστασης Σημείου στον Χώρο από Κάμερα}{58}
\contentsline {chapter}{\numberline {5}Πειράματα και Αποτελέσματα}{63}
\contentsline {section}{\numberline {5.1}Ανίχνευση και Παρακολούθηση Ανθρώπινης Παρουσίας με Χρήση Μίας RGB Κάμερας}{63}
\contentsline {subsection}{\numberline {5.1.1}Εισαγωγή}{63}
\contentsline {subsection}{\numberline {5.1.2}Σετ Δεδομένων CAVIAR}{64}
\contentsline {subsection}{\numberline {5.1.3}Ανιχνευτές Ανθρώπων (Human Detectors)}{64}
\contentsline {subsubsection}{Εύρεση Βέλτιστων Παραμέτρων για Ηaar Cascade και HOG}{65}
\contentsline {subsubsection}{Αποτελέσματα}{66}
\contentsline {subsubsection}{Συμπεράσματα}{69}
\contentsline {subsection}{\numberline {5.1.4}Χρήση Φίλτρων Σωματιδίων για Παρακολούθηση Ανθρώπων}{69}
\contentsline {section}{\numberline {5.2}Ανίχνευση και Παρακολούθηση Ανθρώπινης Παρουσίας με Χρήση Πολλαπλών RBG Καμερών}{83}
\contentsline {subsection}{\numberline {5.2.1}Εισαγωγή}{83}
\contentsline {subsection}{\numberline {5.2.2}Σετ Δεδομένων VIPT\_2014}{83}
\contentsline {subsection}{\numberline {5.2.3}Ανίχνευση και Παρακολούθηση σε 3D Χώρο με Χρήση Μίας Κάμερας}{85}
\contentsline {subsection}{\numberline {5.2.4}Ανίχνευση και Παρακολούθηση σε 3D Χώρο με Χρήση Δύο Καμερών}{93}
\contentsline {subsection}{\numberline {5.2.5}Ανίχνευση και Παρακολούθηση σε 3D Χώρο με Χρήση Τριών Καμερών}{97}
\contentsline {chapter}{\numberline {6}Συμπεράσματα και Μελλοντική Εργασία}{103}
\contentsline {section}{\numberline {6.1}Γενικά Συμπεράσματα}{103}
\contentsline {section}{\numberline {6.2}Προβλήματα}{104}
\contentsline {section}{\numberline {6.3}Μελλοντικές Επεκτάσεις}{105}
